from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
import numpy as np
import torchvision
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy

from ax.plot.contour import plot_contour
from ax.plot.trace import optimization_trace_single_method
from ax.service.managed_loop import optimize

from ax.utils.tutorials.cnn_utils import load_mnist, train, evaluate, CNN


class WeightClipper(object):
    def __init__(self, frequency=5):
        self.frequency = frequency

    def __call__(self, module):
        # filter the variables to get the ones you want
        if hasattr(module, 'weight'):
            if type(module) == type(nn.Linear(512, 2)):
                w = module.weight.data
                b = module.bias.data
                b = b.clamp(-1.5, 1.5)
                module.bias.data = b
                w = w.clamp(-1.5, 1.5)
                module.weight.data = w


class Net(nn.Module):
    def __init__(self, input_feature):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_feature, 4096)
        self.dropout = nn.Dropout(p=0.5)
        self.fc2 = nn.Linear(4096, 1000)
        self.fc3 = nn.Linear(1000, 100)
        self.fc4 = nn.Linear(100, 2)
        # self.fc2 = nn.Linear(20, 2)

    def forward(self, x):
        x = self.dropout(F.relu(self.fc1(x)))
        x = self.dropout(F.relu(self.fc2(x)))
        x = self.dropout(F.relu(self.fc3(x)))
        x = F.relu(self.fc4(x))
        # x = self.fc3(x)
        return x


# ## load data
# data_transforms = {
#     'train': transforms.Compose([
#         transforms.CenterCrop(224),
#         transforms.ToTensor(),
#         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
#     ]),
#     'test': transforms.Compose([
#         transforms.CenterCrop(224),
#         transforms.ToTensor(),
#         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
#     ]),
# }
#
# data_dir = '../differrent_scenario_of_inputs/image_size_377_B_P_A_C1'
# model_num = 1
#
# path_to_save_model = '{}_{}'.format(data_dir.split('/')[-1], model_num)
# # path_to_load_model = 'image_size_377_B_P_A_C_1_84_91'
# image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
#                                           data_transforms[x])
#                   for x in ['train', 'test']}
# dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
#                                               shuffle=True)
#                for x in ['train', 'test']}
# dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'test']}
# class_names = image_datasets['train'].classes
#
# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# model_ft = models.alexnet(pretrained=True)
# for param in model_ft.parameters():
#     param.requires_grad = False
# print(model_ft)
# # num_ftrs = model_ft.fc.in_features
# num_ftrs = 9216
# # Here the size of each output sample is set to 2.
# # Alternatively, it can be generalized to nn.Linear(num_ftrs, len(class_names)).
# t = Net(num_ftrs)
# model_ft.classifier = t
# total_best_acc = 85
# # model_ft.load_state_dict(torch.load(path_to_load_model))
# model_ft = model_ft.to(device)
# torch.manual_seed(1234)
#
#
# def evaluate_func(parameters):
#     global model_ft, path_to_save_model, total_best_acc
#     lr = parameters['lr']
#     w1 = parameters['w1']
#     lambda1 = parameters['lambda']
#     momentum = parameters['momentum']
#     t = Net(num_ftrs)
#     model_ft.classifier = t
#     model_ft = model_ft.to(device)
#     # Observe that all parameters are being optimized
#     weight_class = torch.tensor([w1, 1 - w1]).to(device)
#     criterion = nn.CrossEntropyLoss(weight_class)
#     # optimizer_ft = optim.Adam(model_ft.parameters(), lr=lr, weight_decay=lambda1)
#     optimizer_ft = optim.SGD(model_ft.parameters(), lr=lr, momentum=momentum, nesterov=True)
#     exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)
#     constrain = WeightClipper()
#     model_ft, best_acc, best_acc_train = train_model(model_ft, criterion, exp_lr_scheduler, optimizer_ft, dataloaders,
#                                                      lambda1, constrain=constrain,
#                                                      num_epochs=9)
#     if best_acc > total_best_acc:
#         total_best_acc = best_acc
#         path_to_save_model = '{}_{}_{}_weight_{}'.format(path_to_save_model, int(np.floor(best_acc.item() * 100), w1),
#                                                          int(np.floor(best_acc_train.item() * 100)))
#         torch.save(model_ft.state_dict(), path_to_save_model)
#     temp = best_acc.item()
#     print('----------')
#     print('lr:{},w1:{},lambda:{}'.format(lr, w1, lambda1))
#     return temp
#     # Decay LR by a factor of 0.1 every 7 epochs


# def imshow(inp, path_to_save,title=None):
#     """Imshow for Tensor."""
#     inp = inp.numpy().transpose((1, 2, 0))
#     temp = np.max(inp,axis=2)
#     mean = np.array([0.485])
#     std = np.array([0.229])
#     inp = std * temp + mean
#     inp = np.clip(inp, 0, 1)
#     plt.imshow(inp)
#     plt.savefig(path_to_save)
#     if title is not None:
#         plt.title(title)
#     plt.pause(0.001)  # pause a bit so that plots are updated
def imshow(inp, title=None):
    """Imshow for Tensor."""
    inp = inp.numpy().transpose((1, 2, 0))
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    plt.imshow(inp)
    if title is not None:
        plt.title(title)
    plt.pause(0.001)  # pause a bit so that plots are updated


def visualize_model(model, num_images=6):
    was_training = model.training
    model.eval()
    images_so_far = 0
    fig = plt.figure()

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(dataloaders['test']):
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)

            for j in range(inputs.size()[0]):
                images_so_far += 1
                ax = plt.subplot(num_images // 2, 2, images_so_far)
                ax.axis('off')
                ax.set_title('predicted: {}'.format(class_names[preds[j]]))
                imshow(inputs.cpu().data[j])

                if images_so_far == num_images:
                    model.train(mode=was_training)
                    return
        model.train(mode=was_training)


def train_model(model, criterion, scheduler, optimizer, dataloaders, lambda2, constrain, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and testidation phase
        for phase in ['train', 'test']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        l2_reg = torch.tensor(0.).to(device)
                        for param in model.parameters():
                            if param.requires_grad == True:
                                l2_reg += torch.norm(param)
                        loss = criterion(outputs, labels) + lambda2 * l2_reg
                        loss.backward()
                        optimizer.step()
                        model.apply(constrain)

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)
            if phase == 'train':
                scheduler.step()
                epoch_loss_train = running_loss / dataset_sizes[phase]
                epoch_acc_train = running_corrects.double() / dataset_sizes[phase]
                print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                    phase, epoch_loss_train, epoch_acc_train))
            if phase == 'test':
                epoch_loss = running_loss / dataset_sizes[phase]
                epoch_acc = running_corrects.double() / dataset_sizes[phase]
                print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                    phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'test' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_acc_train = epoch_acc_train
                best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model, best_acc, best_acc_train


if __name__ == '__main__':
    # load data
    data_transforms = {
        'train': transforms.Compose([
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'test': transforms.Compose([
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'validation': transforms.Compose([
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    }

    data_dir = 'Images/image_size_377_A_PBC_validation'
    model_num = 1

    path_to_save_model = '{}_{}'.format(data_dir.split('/')[-1],model_num)
    path_to_load_model = 'image_size_377_with_new_neg_alexnet_6_4_weight_v1'
    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x),
                                              data_transforms[x])
                      for x in ['train', 'validation' , 'test']}
    # merged_dataset = torch.utils.data.ConcatDataset([
    #     image_datasets['train'],
    #     image_datasets['validation'],
    # ])
    # image_datasets['train'] = merged_dataset
    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=4,
                                                  shuffle=True)
                   for x in ['train','test']}
    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'validation' ,'test']}
    class_names = image_datasets['test'].classes

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Get a batch of training data
    inputs, classes = next(iter(dataloaders['train']))

    # Make a grid from batch
    # out = torchvision.utils.make_grid(inputs)

    # imshow(out, title=[class_names[x] for x in classes])

    # load pretrained model

    model_ft = models.alexnet(pretrained=True)
    for param in model_ft.parameters():
        param.requires_grad = False
    print(model_ft)
    # num_ftrs = model_ft.fc.in_features
    num_ftrs = 9216
    # Here the size of each output sample is set to 2.
    # Alternatively, it can be generalized to nn.Linear(num_ftrs, len(class_names)).
    # outputs = model_ft(inputs)
    t = Net(num_ftrs)
    model_ft.classifier = t
    # path_p = 'p'
    # path_n = 'n'
    # counter = 0
    # t = os.path.isdir(path_p)
    # for inputs, labels in dataloaders['train']:
    #     outputs = model_ft(inputs)
    #     image_name = '/{}.png'.format(counter)
    #     counter+=1
    #     if labels == 1:
    #         imshow(torchvision.utils.make_grid(outputs),path_p + image_name)
    #     else:
    #         imshow(torchvision.utils.make_grid(outputs), path_n + image_name)

    # model_ft.load_state_dict(torch.load(path_to_load_model))

    model_ft = model_ft.to(device)
    weight_class = torch.tensor([0.67,1-0.67]).to(device)
    criterion = nn.CrossEntropyLoss(weight_class)

    # Observe that all parameters are being optimized
    optimizer_ft = optim.SGD(model_ft.parameters(), lr=0.003, momentum=0.9)

    # Decay LR by a factor of 0.1 every 7 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=7, gamma=0.1)

    constrain = WeightClipper()
    model_ft,best_acc,best_acc_train = train_model(model_ft, criterion, exp_lr_scheduler ,optimizer_ft,dataloaders,lambda2=0.047,constrain=constrain,
                           num_epochs=20)
    print(type(best_acc))
    path_to_save_model = '{}_{}_{}'.format(path_to_save_model,int(np.floor(best_acc.item()*100)),int(np.floor(best_acc_train.item()*100)))
    torch.save(model_ft.state_dict(),path_to_save_model)

    visualize_model(model_ft)
    # best_parameters, values, experiment, model = optimize(parameters=[
    #     {"name": "lr", "type": "range", "bounds": [0.001, 0.005]},
    #     {"name": "momentum", "type": "range", "bounds": [0.7, 0.9]},
    #     {"name": "w1", "type": "range", "bounds": [0.5, 1.0]},
    #     {"name": "lambda", "type": "range", "bounds": [0.03, 0.05]}
    # ], evaluation_function=evaluate_func, objective_name='Acc_test', arms_per_trial=1, minimize=False, total_trials=10)

