import torch
from torch.utils.data import DataLoader
import numpy as np
from torchvision import transforms,utils
import torchvision.datasets as datasets
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


from torchvision.utils import save_image
import os

# define the name of the directory to be created
# the path to raw data
load_data_path = 'A_P_B_C_2'
# save_transformed_data = 'Test_image_to_preprocess\\224center\\P'
path_to_make_dataset = "differrent_scenario_of_inputs"
name_of_datasets = "/A_P_B_C_4_validation"
train_n = path_to_make_dataset + name_of_datasets + "/train/A"
train_p = path_to_make_dataset + name_of_datasets + "/train/PBC"
test_n  = path_to_make_dataset + name_of_datasets + "/test/A"
test_p  = path_to_make_dataset + name_of_datasets + "/test/PBC"
validation_n = path_to_make_dataset + name_of_datasets + "/validation/A"
validation_p = path_to_make_dataset + name_of_datasets + "/validation/PBC"

# make dir
if not os.path.isdir(train_n):
    os.makedirs(train_n)
    os.makedirs(train_p)
    os.makedirs(test_n)
    os.makedirs(test_p)
    os.makedirs(validation_p)
    os.makedirs(validation_n)

#     print ("Creation of the directory %s failed")
# else:
#     print ("Successfully created the directory %s ")
batch_size = 1
test_per = 0.2
val_per = 0.2
train_transforms = transforms.Compose([transforms.Resize((377,377)),
                                       transforms.ToTensor()])
dataset = datasets.ImageFolder(root=load_data_path,
                                     transform=train_transforms)
datasets = DataLoader(dataset=dataset,
                      batch_size=batch_size,
                      shuffle=False,
                      num_workers=0)
data_length = len(datasets)
train_sets, val_sets, test_sets = torch.utils.data.random_split(dataset, [np.floor(data_length*(1-test_per-val_per)).astype(int),np.floor(data_length*(val_per)).astype(int), np.ceil(data_length*(test_per)).astype(int)])
# train_sets, val_sets, test_sets = torch.utils.data.random_split(dataset, [data_length-600, 300, 300])

# for counter,data in enumerate(datasets):
#     a = dataset.imgs[counter][0].split('\\')
#     save_image(data[0],'{}/{}'.format(save_transformed_data,a[-1]))
for counter,data in enumerate(train_sets):
    if data[1] == 0:
        save_image(data[0],'{}/imag{}.jpg'.format(train_n,counter))
    else:
        save_image(data[0], '{}/imag{}.jpg'.format(train_p,counter))
for counter,data in enumerate(test_sets):
    if data[1] == 0:
        save_image(data[0],'{}/imag{}.jpg'.format(test_n,counter))
    else:
        save_image(data[0], '{}/imag{}.jpg'.format(test_p,counter))
for counter,data in enumerate(val_sets):
    if data[1] == 0:
        save_image(data[0],'{}/imag{}.jpg'.format(validation_n,counter))
    else:
        save_image(data[0], '{}/imag{}.jpg'.format(validation_p,counter))





# functions to show an image
def imshow(img):
    img = img      # unnormalize
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()


# get some random training images
dataiter = iter(trainloader)
images, labels = dataiter.next()

# show images
imshow(utils.make_grid(images))
# print labels
print(' '.join('%5s' % labels[j] for j in range(batch_size)))

