import torch
from torch.utils.data import DataLoader
import numpy as np
from torchvision import transforms,utils
import torchvision.datasets as datasets
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


from torchvision.utils import save_image
import os

# define the name of the directory to be created
load_data_path = 'data/test_image_to_reshape'
save_path_reshaped = 'data/reshaped_image'
save_croped_data = 'data/cropped_data'

# if not os.path.isdir(save_path_reshaped):
#     os.makedirs(save_path_reshaped)
#
# if not os.path.isdir(save_croped_data):
#     os.makedirs(save_croped_data)
#
#
# #     print ("Creation of the directory %s failed")
# # else:
# #     print ("Successfully created the directory %s ")
# batch_size = 1
#
# train_transforms = transforms.Compose([transforms.Resize(size = (377,377)),
#                                        transforms.ToTensor()])
# dataset = datasets.ImageFolder(root=load_data_path,
#                                      transform=train_transforms)
# date_length = len(dataset)
# train_sets, test_sets = torch.utils.data.random_split(dataset, [np.floor(date_length*(1-test_per)).astype(int), np.ceil(date_length*(test_per)).astype(int)])
i=1
# for counter,data in enumerate(dataset):
#     if data[1] == 0:
#         save_image(data[0],'{}/test_image_reshaped.jpg'.format(save_path_reshaped))
#     else:
#         save_image(data[0], '{}/test_image_reshaped.jpg'.format(save_path_reshaped))

train_transforms = transforms.Compose([transforms.CenterCrop(size = (224,224)),
                                       transforms.ToTensor()])
dataset = datasets.ImageFolder(root=save_path_reshaped,
                                     transform=train_transforms)
for counter,data in enumerate(dataset):
    if data[1] == 0:
        save_image(data[0],'{}/test_image_reshaped.jpg'.format(save_croped_data ))
    else:
        save_image(data[0], '{}/test_image_reshaped.jpg'.format(save_croped_data ))




# batch_size = 1
# lambda_l2 = 0.01
# transforms = transforms.Compose([transforms.CenterCrop(100),
#                                  transforms.ToTensor(),
#                                  transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
#
# train_sets = datasets.ImageFolder(root="train_resized",
#                                      transform=transforms)

# matrix_data = torch.zeros([619,3*100*100])
# for counter,i in enumerate(train_sets):
#     image = i[0]
#     matrix_data[counter] = torch.reshape(image,(100*100*3,1))
#
# test_sets = datasets.ImageFolder(root="test_resized",
#                                      transform=transforms)


# trainloader = DataLoader(dataset=train_sets,
#                       batch_size=batch_size,
#                       shuffle=True,
#                       num_workers=0)
#
# testloader = DataLoader(dataset=test_sets,
#                       batch_size=batch_size,
#                       shuffle=False,
#                       drop_last= True,
#                       num_workers=0)
#
#
#
#
# # functions to show an image
#
#
# def imshow(img):
#     img = img      # unnormalize
#     npimg = img.numpy()
#     plt.imshow(np.transpose(npimg, (1, 2, 0)))
#     plt.show()
#
#
# # get some random training images
# dataiter = iter(trainloader)
# images, labels = dataiter.next()
#
# # show images
# imshow(utils.make_grid(images))
# # print labels
# print(' '.join('%5s' % labels[j] for j in range(batch_size)))

